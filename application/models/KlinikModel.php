<?php 

class KlinikModel extends CI_Model {

	function view($table){
		return $this->db->query("select * from $table");
	}
	function view_usia($table,$usia1,$usia2){
		return $this->db->query("select * from $table where usia>=$usia1 and usia<=$usia2");
	}
	function view_jk($table,$jk){
		return $this->db->query("select * from $table where jenis_kelamin='$jk'");
	}
	function view_join($table1,$table2,$table3,$foregn,$foregn2,$where,$value){
		return $this->db->query("select * from $table1 inner join $table2 on $table1.$foregn=$table2.$foregn inner join $table3 on $table2.$foregn2=$table3.$foregn2 where $table2.$where='$value'");
	}
	function ps_laki(){
		return $this->db->query("select * from pasien p join periksa pr on p.ps_id=pr.ps_id WHERE p.jenis_kelamin='L' and pr.status='0'");
	}
	function ps_perempuan(){
		return $this->db->query("select * from pasien p join periksa pr on p.ps_id=pr.ps_id WHERE p.jenis_kelamin='P' and pr.status='0'");
	}
	function antrian(){
		$date= date("Y-m-d");
		return $this->db->query("select * from periksa where tgl_periksa='$date'");
	}
	function tertangani(){
		$date= date("Y-m-d");
		return $this->db->query("select * from periksa where tgl_periksa='$date' and status='1'");
	}
	function input_data($data,$table){
		$this->db->insert($table,$data);
	}

	function hapus_data($where,$table){
		$this->db->where($where);
		$this->db->delete($table);
	}

	function edit_data($table,$where){		
		return $this->db->get_where($table,$where);
	}
 
	function update_data($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}
}
