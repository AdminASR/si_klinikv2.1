<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Msr_dokter extends CI_Controller {
	
	public function __construct(){
	parent::__construct();
	$this->load->model('KlinikModel');
	}
	
	public function index(){
		$data['dokter'] = $this->KlinikModel->view('dokter')->result();
		$this->load->view('dokter/header.php');
		$this->load->view('dokter/msr_dokter',$data);
		$this->load->view('dokter/footer.php');
	}

	public function tambah(){
		$this->load->view('dokter/header.php');
		$this->load->view('dokter/msr_dokter_tambah');
		$this->load->view('dokter/footer.php');
	}
	public function fungsi_tambah(){
		$nama = $this->input->post('nama');
		$jenis_kelamin = $this->input->post('jenis_kelamin');
		$tgl_lahir = $this->input->post('tgl_lahir');
		$alamat = $this->input->post('alamat');
		$telp = $this->input->post('telp');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
	 
			$data = array(
			'nama' => $nama,
			'username' => $username,
			'password' => $password
				);
			$this->KlinikModel->input_data($data,'dokter');
			redirect('msr_dokter');
	}
	public function edit($dr_id){
		$where = array('dr_id' => $dr_id);
		$data['dokter'] = $this->KlinikModel->edit_data('dokter',$where)->result();
		$this->load->view('dokter/header.php');
		$this->load->view('dokter/msr_dokter_edit',$data);
		$this->load->view('dokter/footer.php');
	}
	public function update(){
		$dr_id = $this->input->post('dr_id');
		$nama = $this->input->post('nama');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
	 
			$data = array(
			'nama' => $nama,
			'username' => $username,
			'password' => $password
				);

			$where = array(
				'dr_id' => $dr_id
			);
			$this->KlinikModel->update_data($where,$data,'dokter');
			redirect('msr_dokter');
	}

	public function hapus($dr_id){
		$where = array('dr_id' => $dr_id);
		$this->KlinikModel->hapus_data($where,'dokter');
		redirect('msr_dokter');
	}
}
