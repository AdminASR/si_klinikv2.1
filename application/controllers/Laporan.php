<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Laporan extends CI_Controller {
	
	public function __construct(){
	parent::__construct();
	$this->load->model('KlinikModel');
	}
	
	public function bulanan(){
		$data['balita'] = $this->KlinikModel->view_usia("pasien","0","5")->num_rows();
		$data['anak'] = $this->KlinikModel->view_usia("pasien","5","11")->num_rows();
		$data['remaja'] = $this->KlinikModel->view_usia("pasien","12","25")->num_rows();
		$data['dewasa'] = $this->KlinikModel->view_usia("pasien","26","45")->num_rows();
		$data['manula'] = $this->KlinikModel->view_usia("pasien","46","150")->num_rows();
		$data['laki'] = $this->KlinikModel->view_jk("pasien","L")->num_rows();
		$data['perempuan'] = $this->KlinikModel->view_jk("pasien","P")->num_rows();
		$this->load->view('dokter/header.php');
		$this->load->view('dokter/lpr_bulanan',$data);
		$this->load->view('dokter/footer.php');
	}
	public function harian(){
		$date=$this->input->post("tanggal");
		if ($date==NULL) {
			$date=date("Y-m-d");
		}
		$data['periksa'] = $this->KlinikModel->view_join("pasien","periksa","periksa_gejala","ps_id","pr_id","tgl_periksa",$date)->result();
		$this->load->view('dokter/header.php');
		$this->load->view('dokter/lpr_harian',$data);
		$this->load->view('dokter/footer.php');
	}
}
