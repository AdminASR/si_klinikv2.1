<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->model('M_login');
		
		// $this->load->model('SiswaModel'); // Load SiswaModel ke controller ini
	}
	public function dktr(){
		$this->load->view('d_login');
	}

	public function dktr_login(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$where = array(
			'username' => $username,
			'password' => md5($password)
			 );
		$cek_dktr = $this->M_login->cek_login("dokter",$where)->num_rows();
		if($cek_dktr > 0){
			$data_session = array(
				'nama' 		=> $username,
				'status' 	=> "login_dktr"
			);

			$this->session->set_userdata($data_session);

			redirect(base_url("daftar"));
		}else{
			echo "Username / Password Salah";
		}
	}


	public function rspsns(){
		$this->load->view('r_login');
	}
	public function rspsns_login(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$where = array(
			'username' => $username ,
			'password' => md5($password)
			 );
		$cek_rspsns = $this->M_login->cek_login("resepsionis",$where)->num_rows();
		if($cek_rspsns > 0){
			$data_session = array(
				'nama' 		=> $username,
				'status' 	=> "login_rspsns"
			);

			$this->session->set_userdata($data_session);

			redirect(base_url("daftar"));
		}else{
			echo "Username / Password Salah";
		}
	}
	public function logout(){
		$this->session->sess_destroy();
		if($this->session->userdata('status') == 'login_dktr'){
			redirect('login/dktr');
		}elseif ($this->session->userdata('status') == 'login_rspsns') {
			redirect('login/rspsns');
		}
		
		
	}

	public function s_acc_dktr($dr_id){
		$where = array('dr_id' =>$dr_id , );
		$data['acc_dktr'] = $this->M_login->edit_data($where,'dokter')->result();
		$this->load->view('header',$data);
		$this->load->view('dokter/edit_akun',$data);
		$this->load->view('footer');
	}

	public function s_acc_rspsns($rs_id){
		$where = array('rs_id' =>$rs_id , );
		$data['acc_rspsns'] = $this->M_login->edit_data($where,'resepsionis')->result();
		$this->load->view('header',$data);
		$this->load->view('dokter/edit_akun',$data);
		$this->load->view('footer');
	}


}
