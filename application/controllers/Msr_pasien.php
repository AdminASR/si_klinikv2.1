<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Msr_pasien extends CI_Controller {
	
	public function __construct(){
	parent::__construct();
	$this->load->model('KlinikModel');
	}
	
	public function index(){
		$data['pasien'] = $this->KlinikModel->view('pasien')->result();
		$this->load->view('dokter/header.php');
		$this->load->view('dokter/msr_pasien',$data);
		$this->load->view('dokter/footer.php');
	}
	public function edit($ps_id){
		$where = array('ps_id' => $ps_id);
		$data['pasien'] = $this->KlinikModel->edit_data('pasien',$where)->result();
		$this->load->view('dokter/header.php');
		$this->load->view('dokter/msr_pasien_edit',$data);
		$this->load->view('dokter/footer.php');
	}
	public function update(){
		$ps_id = $this->input->post('ps_id');
		$nama = $this->input->post('nama');
		$jenis_kelamin = $this->input->post('jenis_kelamin');
		$usia = $this->input->post('usia');
		$telp = $this->input->post('telp');
		$alamat = $this->input->post('alamat');
	 
			$data = array(
			'nama' => $nama,
			'jenis_kelamin' => $jenis_kelamin,
			'usia' => $usia,
			'telp' => $telp,
			'alamat' => $alamat
				);

			$where = array(
				'ps_id' => $ps_id
			);
			$this->KlinikModel->update_data($where,$data,'pasien');
			redirect('msr_pasien');
	}

	public function hapus($ps_id){
		$where = array('ps_id' => $ps_id);
		$this->KlinikModel->hapus_data($where,'pasien');
		redirect('msr_pasien');
	}
}
