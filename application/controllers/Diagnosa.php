<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Diagnosa extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
			if($this->session->userdata('status') == NULL){
			redirect(base_url("login/dktr"));
		}
	}
	
	public function index(){
		$this->load->view('header.php');
		$this->load->view('dokter/diagnosa');
		$this->load->view('footer.php');
	}

}
