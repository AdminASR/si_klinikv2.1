<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Antrian extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->model('M_Antrian');
		if($this->session->userdata('status') == NULL){
			redirect(base_url("login/dktr"));
		}
	   
	}
	
	public function index(){
		$data['antrian'] = $this->M_Antrian->l_antri()->result();
		$data['antrian_by'] = $this->M_Antrian->d_antri()->result();
		$this->load->view('dokter/header.php');
		$this->load->view('dokter/antrian',$data);
		$this->load->view('dokter/footer.php');
	}

	public function status_next(){
		$pr_id = $this->input->post('pr_id');
		$status = $this->input->post('status');

		$data = array('status' =>'1' , );

		$where = array('pr_id' =>$pr_id , );

		$this->M_Antrian->update_data($where,$data,'periksa');
		redirect('antrian');
	}
	public function status_prev(){
		$pr_id = $this->input->post('pr_id');
		$status = $this->input->post('status');

		$data = array('status' =>$status , );

		$where = array('pr_id' =>$pr_id , );

		$this->M_Antrian->update_data($where,$data,'periksa');
		redirect('antrian');
	}


	public function diagnosa($pr_id){
		$data['diagnosa'] = $this->M_Antrian->diagnosa($pr_id)->result();
		$this->load->view('dokter/header.php');
		$this->load->view('dokter/diagnosa',$data);
		$this->load->view('dokter/footer.php');

	}
	
// 	function tambah_peminjam(){
// 	$nis = $this->input->post('nis');
// 	$posisinis1=strpos($nis,"(")+1;
// 	$posisinis2=strpos($nis,")");
// 	$ceknis=$posisinis2-$posisinis1;
// 	$nis=substr($nis,$posisinis1,$ceknis);
// 	$keterangan = $this->input->post('keterangan');
// 	$tgl_pinjam = $this->input->post('tgl_pinjam');
// 	$tgl_kembali = $this->input->post('tgl_kembali');
// 	$status = $this->input->post('status');
// 	$cek=$this->db->query('select max(id_pinjam) as id_pinjam from peminjam')->result();
// 	$kode=0;
// 	foreach ($cek as $row){
// 		$kode=$row->id_pinjam+1;
// 	}
// 		$data = array(
// 		'id_pinjam'=>$kode,
// 		'nis' => $nis,
// 		'tgl_pinjam' => $tgl_pinjam,
// 		'tgl_kembali' => $tgl_kembali,
// 		'keterangan' => $keterangan,
// 		'status' => $status
// 			);
// 		$this->AdminModel->input_data($data,'peminjam');
		
// 		   $i=0;
// 		   $n=$this->input->post('idf');
// 		   while($i<$n){
// 			$kode_buku = $_POST['kode_buku'][$i];
// 			$jml = $_POST['jml'][$i];
// 			$a=strpos($kode_buku,"(")+1;
// 			$b=strpos($kode_buku,")");
// 			$c=$b-$a;
// 			$kode_buku=substr($kode_buku,$a,$c);
// 			$cek_stok=$this->db->query("select jumlah from buku where kode_buku='$kode_buku'")->result();
// 			$jml1=0;
// 			foreach ($cek_stok as $row1){
// 		$jml1=$row1->jumlah;
// 		$upstok=$jml1-$jml;
// 	}
// 			$data_b=array(
// 				'jumlah'=>$upstok
// 			);
// 			$where=array(
// 				'kode_buku'=>$kode_buku
// 			);
// 			$this->AdminModel->update_data_b($where,$data_b,'buku');
// //cek stok buku berdasarkan kode, tampiljkan hasil stok, kurangi stok dengan yang dipinjam.
// 			//update tbl buku, stok
// 			 $data = array(
// 			'id_pinjam' => $kode,
// 			'kode_buku' => $kode_buku,
// 			'jml' => $jml
// 			);
// 			$this->AdminModel->input_data($data,'detail_peminjaman');
// 		   $i++;
// 		   }
		
// 		redirect('peminjam');
// 	} 

	public function resep(){
		$this->load->view('dokter/resep');
	}

	

}
