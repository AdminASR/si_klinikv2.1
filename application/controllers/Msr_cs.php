<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Msr_cs extends CI_Controller {
	
	public function __construct(){
	parent::__construct();
	$this->load->model('KlinikModel');
	}
	
	public function index(){
		$data['resepsionis'] = $this->KlinikModel->view('resepsionis')->result();
		$this->load->view('dokter/header.php');
		$this->load->view('dokter/msr_cs',$data);
		$this->load->view('dokter/footer.php');
	}
	public function tambah(){
		$this->load->view('dokter/header.php');
		$this->load->view('dokter/msr_cs_tambah');
		$this->load->view('dokter/footer.php');
	}

	public function fungsi_tambah(){
		$nama = $this->input->post('nama');
		$jenis_kelamin = $this->input->post('jenis_kelamin');
		$tgl_lahir = $this->input->post('tgl_lahir');
		$alamat = $this->input->post('alamat');
		$telp = $this->input->post('telp');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
	 
			$data = array(
			'nama' => $nama,
			'username' => $username,
			'password' => $password
				);
			$this->KlinikModel->input_data($data,'resepsionis');
			redirect('msr_cs');
	}
	public function edit($rs_id){
		$where = array('rs_id' => $rs_id);
		$data['resepsionis'] = $this->KlinikModel->edit_data('resepsionis',$where)->result();
		$this->load->view('dokter/header.php');
		$this->load->view('dokter/msr_cs_edit',$data);
		$this->load->view('dokter/footer.php');
	}
	public function update(){
		$rs_id = $this->input->post('rs_id');
		$nama = $this->input->post('nama');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
	 
			$data = array(
			'nama' => $nama,
			'username' => $username,
			'password' => $password
				);

			$where = array(
				'rs_id' => $rs_id
			);
			$this->KlinikModel->update_data($where,$data,'resepsionis');
			redirect('msr_cs');
	}

	public function hapus($rs_id){
		$where = array('rs_id' => $rs_id);
		$this->KlinikModel->hapus_data($where,'resepsionis');
		redirect('msr_cs');
	}
}
