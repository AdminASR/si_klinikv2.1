<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Daftar extends CI_Controller {
	
	public function __construct(){
	parent::__construct();
	$this->load->model('KlinikModel'); // Load SiswaModel ke controller ini
	}
	
	function index(){
		$data['laki']=$this->KlinikModel->ps_laki()->num_rows();
		$data['perempuan']=$this->KlinikModel->ps_perempuan()->num_rows();
		$data['antrian']=$this->KlinikModel->antrian()->num_rows();
		$data['tertangani']=$this->KlinikModel->tertangani()->num_rows();
		$this->load->view('dokter/header.php');
		$this->load->view('dokter/daftar',$data);
		$this->load->view('dokter/footer.php');
	}

	function tambah_pasien(){
	$nama = $this->input->post('nama');
	$jenis_kelamin = $this->input->post('jenis_kelamin');
	$usia = $this->input->post('usia');
	$telp = $this->input->post('telp');
	$alamat = $this->input->post('alamat');
	$tgl_daftar = $this->input->post('tgl_daftar');
	$tgl_periksa = $this->input->post('tgl_periksa');
	$keluhan = $this->input->post('keluhan');
	$ps_id_max = $this->db->query("select max(ps_id) as ps_id from pasien")->result();
	foreach ($ps_id_max as $row){
		$ps_id=$row->ps_id+1;
	}
	$data = array(
			'ps_id' => $ps_id,
			'nama' => $nama,
			'jenis_kelamin' => $jenis_kelamin,
			'usia' => $usia,
			'alamat' => $alamat,
			'telp' => $telp,
			'tgl_daftar' => $tgl_daftar
			);
			$this->KlinikModel->input_data($data,'pasien');
	$id_max=$this->db->query("SELECT MAX(pr_id) as id FROM periksa")->result();
	foreach ($id_max as $id1){
		$id_max2= $id1->id;
	}
	$id_max_data=$this->db->query("select * from periksa where pr_id='$id_max2'")->result();
	foreach ($id_max_data as $id){
		$tgl_akhir=$id->tgl_periksa;
		$antrian_akhir=$id->antrian;
	}
	$antrian=1;
		if ($tgl_periksa==$tgl_akhir) {
			$antrian=$antrian_akhir+1;
		}
	$data = array(
			'tgl_periksa' => $tgl_periksa,
			'rs_id' => 1,
			'ps_id' => $ps_id,
			'dr_id' => 1,
			'antrian' => $antrian,
			'status' => 0
			);
			$this->KlinikModel->input_data($data,'periksa');
			$pr_id_max = $this->db->query("select max(pr_id) as pr_id from periksa")->result();
	foreach ($pr_id_max as $row){
		$pr_id=$row->pr_id+1;echo "$pr_id";
	}
			$i=0;
			$n=$this->input->post('idp');
			while($i<$n){
			$gejala = $_POST['keluhan'][$i];
			 $data = array(
			'pr_id' => $pr_id,
			'gejala' => $gejala
			);
			$this->KlinikModel->input_data($data,'periksa_gejala');
		   $i++;
		}
		

		redirect("daftar");
	}

}
