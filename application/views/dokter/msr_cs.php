
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
     <section class="content-header">
        <h1><i class="fa fa-user-plus"></i> Master
        <i class="fa fa-angle-right"></i><small> Customer Service</small>
         </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data CS</h3>
              <a href="<?php echo base_url('msr_cs/tambah')?>"><button class="pull-right btn btn-success"><i class="fa fa-user-plus"></i> </button></a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>NO</th>
                  <th>Nama</th>
                  <th><center>Aksi</center></th>
                </tr>
                </thead>
                <tbody>
                  <?php 
                  $n=1;
                  foreach ($resepsionis as $p) {
                   ?>
                <tr>
                  <td><?php echo $n++; ?></td>
                  <td><?php echo $p->nama; ?></td>
                  <td align="center">
                    <?php echo anchor('msr_cs/edit/'.$p->rs_id,'<button class="btn btn-success"><i class="fa fa-edit"></i> </button>'); ?>
                    <button class="btn btn-primary"><i class="fa fa-file-text"></i> </button> 
                    <?php echo anchor('msr_cs/hapus/'.$p->rs_id,'<button class="btn btn-danger"><i class="fa fa-trash"></i> </button>'); ?>
                  </td>
                </tr>
              <?php } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>NO</th>
                  <th>Nama</th>
                  <th><center>Aksi</center></th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        
      </div>
      </section>
    <!-- /.content -->
  </div>

<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url('assetsLTE/bower_components/jquery/dist/jquery.min.js')?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('assetsLTE/bower_components/bootstrap/dist/js/bootstrap.min.js')?>"></script>
<!-- DataTables -->
<script src="<?php echo base_url('assetsLTE/bower_components/datatables.net/js/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assetsLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url('assetsLTE/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('assetsLTE/bower_components/fastclick/lib/fastclick.js')?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assetsLTE/dist/js/adminlte.min.js')?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('assetsLTE/dist/js/demo.js')?>"></script>
<!-- page script -->
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
<script type="text/javascript">
   function tambahBuku() {
     var idfg = document.getElementById("idfg").value;
     var stre;
     var stre1;
     var asr=1;
     stre="<p id='srow" + idfg + "'><br><input type='text' class='form-control' name='keluhan[]'' placeholder='puyeng'>";
     $("#form").append(stre);

     stre1="<p id='srow1" + idfg + "'><br> <button class='btn btn-danger' onclick='hapusElemeng(\"#srow1" + idfg + "\");hapusElemeng(\"#srow" + idfg + "\"); return false;'><i class='fa fa-minus'></i></button>";
     $("#button").append(stre1);
     idfg = (idfg-1) + 2;
     document.getElementById("idfg").value = idfg;
   }
   function hapusElemeng(idfg) {
     $(idfg).remove();
   }
</script>
</body>
</html>