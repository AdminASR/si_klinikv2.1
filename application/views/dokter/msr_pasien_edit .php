

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
     <section class="content-header">
        <h1><i class="fa fa-user-md"></i> Master
        <i class="fa fa-angle-right"></i><small> Dokter</small>
        <small> <i class="fa fa-angle-right"></i>Edit</small>
         </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box box-info">
            <div class="box-header with-border">
              <a href="<?php echo base_url('msr_pasian')?>"><button class="btn btn-success pull-left"><i class="fa fa-mail-reply"></i></button></a>
              <h3 class="box-title"> Edit data</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php 
             foreach($pasien as $p){ 
            ?>
            <form class="form-horizontal" method="post" action="<?php echo base_url('msr_pasien/update'); ?>">
              <div class="box-body">
                <div class="row">
                <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-4">Nama</label>

                  <div class="col-md-8">
                    <input type="text" class="form-control" name="nama" required="" placeholder="Nama Lengkap">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-4">Jenis Kelamin</label>

                  <div class="col-md-8">
                    <input type="radio"  name="jenis_kelamin"  value="L" required="" >Laki-laki
                    <input type="radio"  name="jenis_kelamin"  value="P" required="" >Perempuan
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-4">Usia(tahun)</label>

                  <div class="col-md-8">
                    <input type="number" class="form-control" name="usia" max="250" min="0" placeholder="123">
                  </div>
                </div>
              </div>
                <div class="col-md-6">
                  <div class="form-group">
                  <label class="col-md-3">Telp/HP</label>

                  <div class="col-md-8">
                    <input type="text" class="form-control" name="telp" placeholder="08XXXXXXXXX" >
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3">Alamat</label>

                  <div class="col-md-8">
                    <textarea class="form-control" rows="2" name="alamat" placeholder="Desa, Kecamatan, Kabupaten"></textarea>
                  </div>
                </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-success pull-right">Simpan</button>
              </div>
              <!-- /.box-footer -->
            </form>
          <?php
           }
          ?>
          </div>
          </div>
          <!-- /.box -->
        </div>
        
      </div>
      </section>
    <!-- /.content -->
  </div>

<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url('assetsLTE/bower_components/jquery/dist/jquery.min.js')?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('assetsLTE/bower_components/bootstrap/dist/js/bootstrap.min.js')?>"></script>
<!-- DataTables -->
<script src="<?php echo base_url('assetsLTE/bower_components/datatables.net/js/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assetsLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url('assetsLTE/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('assetsLTE/bower_components/fastclick/lib/fastclick.js')?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assetsLTE/dist/js/adminlte.min.js')?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('assetsLTE/dist/js/demo.js')?>"></script>
<!-- page script -->
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
<script type="text/javascript">
   function tambahBuku() {
     var idfg = document.getElementById("idfg").value;
     var stre;
     var stre1;
     var asr=1;
     stre="<p id='srow" + idfg + "'><br><input type='text' class='form-control' name='keluhan[]'' placeholder='puyeng'>";
     $("#form").append(stre);

     stre1="<p id='srow1" + idfg + "'><br> <button class='btn btn-danger' onclick='hapusElemeng(\"#srow1" + idfg + "\");hapusElemeng(\"#srow" + idfg + "\"); return false;'><i class='fa fa-minus'></i></button>";
     $("#button").append(stre1);
     idfg = (idfg-1) + 2;
     document.getElementById("idfg").value = idfg;
   }
   function hapusElemeng(idfg) {
     $(idfg).remove();
   }
</script>
</body>
</html>