
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
   <section class="content-header">
        <h1><i class="fa fa-user-plus"></i> Diagnosa
        <i class="fa fa-angle-right"></i><small> Keluhan Pasien</small>
         </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-4">
        
          <!-- /.box -->

          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2"  class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                  <td align="center"><b>Keluhan Sebelumnya</b></td>
                </tr>
                </thead>
                <tbody>
               
                <tr>
                  <td>Nyeri </td>
                </tr>
                <tr>
                  <td>Gusi Bengkak</td>
                </tr>


                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
           <div class="col-md-8">
          <!-- Widget: user widget style 1 -->
          <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-green">

              <div class="widget-user-image pull-right">
                <img class="img-circle" src="<?php echo base_url('assetsLTE/dist/img/logo.png')?>" alt="User Avatar" width="100px" height="100px" >
              </div>
               <div class="widget-user-image pull-left">
                <img class="img-circle" src="<?php echo base_url('assetsLTE/dist/img/logo.png')?>" alt="User Avatar" width="100px" height="100px" >
              </div>
             <center>
              <h2><b>Klinik</b></h2>
              <h5 class="widget-user-desc"><b>Kabupaten Unknown</b> </h5>
              </center>
              <br>
            </div>
            <div class="box-footer no-padding">
              <form class="form-horizontal" method="post">
                 <?php foreach($diagnosa as $data) {?>
             <table class="table">
               <tr>
                 <td width="20%">Nama</td>
                 <td>:</td>
                 <th><?php echo $data->nama?></th>
               </tr>
                 <tr>
                 <td>Jenis Kelamin</td>
                 <td>:</td>
                 <th><?php if( $data->jenis_kelamin=='l'){
                  echo "Laki - Laki";
                 }
                 elseif( $data->jenis_kelamin=='p'){
                  echo "Perempuan";
                 }?></th>
               </tr>
                 <tr>
                 <td>Usia</td>
                 <td>:</td>
                 <th><?php echo $data->usia?></th>
               </tr>
               <tr>
              <tr>
                <td>Keluhan</td>
                <td>:</td>
                <th>
                   <div class="form-group">
                   <input id="idfg" name='idfg' value="1" type="hidden" >

                  <div class="col-md-10">
                    <input type="text" class="form-control" name="keluhan[]" placeholder="puyeng" >
                   <div id="form"></div>
                  </div>
                    <button class="btn btn-success " type="button" onclick="tambahBuku(); return false;"><i class="fa fa-plus"></i></button>
                    <div id="button"></div>
                </div>
                </th>
                 
               </tr>
               <tr>
                <td colspan="3">  <a href="<?php echo base_url('antrian/resep')?>" class="btn btn-primary  pull-right" ><i class="fa fa-arrow-right"></i> Next</a></td>
               </tr>
             
             </table>
                <?php  } ?>
           
             </form>
            </div>
          </div>
          <!-- /.widget-user -->
        </div>
        <!-- /.col -->
      </div>

      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url('assetsLTE/bower_components/jquery/dist/jquery.min.js')?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('assetsLTE/bower_components/bootstrap/dist/js/bootstrap.min.js')?>"></script>
<!-- DataTables -->
<script src="<?php echo base_url('assetsLTE/bower_components/datatables.net/js/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assetsLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url('assetsLTE/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('assetsLTE/bower_components/fastclick/lib/fastclick.js')?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assetsLTE/dist/js/adminlte.min.js')?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('assetsLTE/dist/js/demo.js')?>"></script>
<!-- page script -->
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
<script type="text/javascript">
   function tambahBuku() {
     var idfg = document.getElementById("idfg").value;
     var stre;
     var stre1;
     var asr=1;
     stre="<p id='srow" + idfg + "'><br><input type='text' class='form-control' name='keluhan[]'' placeholder='puyeng'>";
     $("#form").append(stre);

     stre1="<p id='srow1" + idfg + "'><br> <button class='btn btn-danger' onclick='hapusElemeng(\"#srow1" + idfg + "\");hapusElemeng(\"#srow" + idfg + "\"); return false;'><i class='fa fa-minus'></i></button>";
     $("#button").append(stre1);
     idfg = (idfg-1) + 2;
     document.getElementById("idfg").value = idfg;
   }
   function hapusElemeng(idfg) {
     $(idfg).remove();
   }
</script>

<!-- 
 <div class="form-group">
                  <label for="exampleInputPassword1">Judul</label><br>
                   <input id="idf" name='idf' value="1" type="hidden" >
                   <input name='status' value="1" type="hidden" >
                   <div class="row">
                   <input type='text'
                                  required="" 
                                   placeholder="--judul buku--"
                                   class='flexdatalist form-control'
                                   data-min-length='1'
                                   data-selection-required='true'
                                   list='buku'
                                   name='kode_buku[]'>

                            <datalist id="buku">
                              <?php 
                              foreach($buku as $p){ 
                              ?>
                                <option><?php echo $p->judul?> (<?php echo $p->kode_buku?>)</option>
                              <?php }?>
                            </datalist>

                   <input type="number" name="jml[]" min="1" placeholder=" --jumlah--">
                   <button class="btn btn-success " type="button" onclick="tambahHobi(); return false;"><i class="fa fa-plus"></i></button>
                   <div id="divHobi"></div>                                
                 </div>
               </div>
               <script type="text/javascript">
   function tambahHobi() {
     var idf = document.getElementById("idf").value;
     var stre;
     var asr=1;
     stre="<p id='srow" + idf + "'><br><input type='text' required='' placeholder='--judul buku--' class='flexdatalist form-control' data-min-length='1' data-selection-required='true' list='buku" + asr + "' name='kode_buku[]'><datalist id='buku" + asr + "'><?php 
                              foreach($buku as $p){ 
                              ?>
                                <option><?php echo $p->judul?> (<?php echo $p->kode_buku?>)</option><?php }?>
                            </datalist><input type='number' name='jml[]' min='1' size='20' placeholder='--jumlah--'><a href='#' style=\"color:#3399FD;\" onclick='hapusElemen(\"#srow" + idf + "\"); return false;'> <button class='btn btn-danger'><i class='fa fa-minus'></i></button></a>";
     $("#divHobi").append(stre);
     idf = (idf-1) + 2;
     document.getElementById("idf").value = idf;
   }
   function hapusElemen(idf) {
     $(idf).remove();
   }
   function tambahBuku() {
     var idfg = document.getElementById("idfg").value;
     var stre;
     var asr=1;
     stre="<p id='srow" + idfg + "'><br><input type='text' required='' placeholder='--judul buku--' class='flexdatalist form-control' data-min-length='1' data-selection-required='true' list='buku" + asr + "' name='kode_buku[]'><datalist id='buku" + asr + "'><?php 
                              foreach($buku as $p){ 
                              ?>
                                <option><?php echo $p->judul?> (<?php echo $p->kode_buku?>)</option><?php }?>
                            </datalist><input type='number' name='jml[]' min='1' size='20' placeholder='--jumlah--'><a href='#' style=\"color:#3399FD;\" onclick='hapusElemeng(\"#srow" + idfg + "\"); return false;'> <button class='btn btn-danger'><i class='fa fa-minus'></i></button></a>";
     $("#divBuku").append(stre);
     idfg = (idfg-1) + 2;
     document.getElementById("idfg").value = idfg;
   }
   function hapusElemeng(idfg) {
     $(idfg).remove();
   }
</script> -->