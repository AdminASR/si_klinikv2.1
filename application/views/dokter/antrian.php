
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1><i class="fa fa-user-plus"></i> Antrian
        <i class="fa fa-angle-right"></i><small> Data Pasien</small>
         </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-4">
        
          <!-- /.box -->
           <div class="small-box bg-green">
            <div class="inner">

              <?php
              foreach($antrian_by as $data) { ?>

                <h3><?php echo $data->antrian?></h3>
                <p><?php echo $data->nama?></p>     
                <?php  }
              ?>
            </div>
            <div class="icon">
              <i class="fa fa-stethoscope"></i>
            </div>
        
          </div>

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Antrian</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table  class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                   <th>Antrian</th>
                  <th>Nama Pasien</th>
                  <th>aksi</th>
                </tr>
                </thead>
                <tbody>
            <?php
              foreach($antrian as $data) {?>
                 <tr>
                  <td><?php echo $data->antrian?></td>
                  <td><?php echo $data->nama?></td>
                  <td>
                    <form action="<?php echo base_url('antrian/status_next/')?>" method="post">
                      <input type="hidden" name="pr_id" value="<?php echo $data->pr_id ?>">
                      <input type="hidden" name="status" value="<?php echo $data->status ?>">
                      <button type="submit" value="Simpan" class="btn btn-primary">Next <i class="fa fa-angle-right"></i></button>
                    </form>
                  </td>
                </tr>

            <?php  }
              ?>
                </tbody>
                <tfoot>
                <tr>
                   <th>No Antrian</th>
                  <th>Nama</th>
                  <th>aksi</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
           <div class="col-md-8">
          <!-- Widget: user widget style 1 -->
     
          <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-green">

              <div class="widget-user-image pull-right">
                <img class="img-circle" src="<?php echo base_url('assetsLTE/dist/img/logo.png')?>" alt="User Avatar" width="100px" height="100px" >
              </div>
               <div class="widget-user-image pull-left">
                <img class="img-circle" src="<?php echo base_url('assetsLTE/dist/img/logo.png')?>" alt="User Avatar" width="100px" height="100px" >
              </div>
             <center>
              <h2><b>Klinik</b></h2>
              <h5 class="widget-user-desc"><b>Kabupaten Unknown</b> </h5>
              </center>
              <br>
            </div>
            <div class="box-footer no-padding">
           <?php foreach($antrian_by as $data) {?>
             <table class="table">
               <tr>
                 <td width="20%">Nama</td>
                 <td>:</td>
                 <th><?php echo $data->nama?></th>
               </tr>
                 <tr>
                 <td>Jenis Kelamin</td>
                 <td>:</td>
                 <th><?php if( $data->jenis_kelamin=='l'){
                  echo "Laki - Laki";
                 }
                 elseif( $data->jenis_kelamin=='p'){
                  echo "Perempuan";
                 }?></th>
               </tr>
                 <tr>
                 <td>Usia</td>
                 <td>:</td>
                 <th><?php echo $data->usia?></th>
               </tr>
               <tr>
                  <td>
                   <form action="<?php echo base_url('antrian/status_prev/')?>" method="post">
                      <input type="hidden" name="pr_id" value="<?php echo $data->pr_id ?>">
                      <input type="hidden" name="status" value="0">
                      <button type="submit" value="Simpan" class="btn btn-danger pull-left">Batal <i class="fa fa-close"></i></button>
                    </form>
                 </td>
               	<td colspan="3">  <a href="<?php echo base_url('antrian/diagnosa/'.$data->pr_id)?>" class="btn btn-primary  pull-right" >Next <i class="fa fa-arrow-right"></i></a></td>
               
               </tr>
             
             </table>
                <?php  } ?>
             
            </div>
          </div>
      
          <!-- /.widget-user -->
        </div>
        <!-- /.col -->
      </div>

      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url('assetsLTE/bower_components/jquery/dist/jquery.min.js')?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('assetsLTE/bower_components/bootstrap/dist/js/bootstrap.min.js')?>"></script>
<!-- DataTables -->
<script src="<?php echo base_url('assetsLTE/bower_components/datatables.net/js/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assetsLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url('assetsLTE/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('assetsLTE/bower_components/fastclick/lib/fastclick.js')?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assetsLTE/dist/js/adminlte.min.js')?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('assetsLTE/dist/js/demo.js')?>"></script>
<!-- page script -->
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
