
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1><i class="fa fa-user-plus"></i> Laporan
        <i class="fa fa-angle-right"></i><small> Bulanan</small>
         </h1>
    </section>

    <!-- Main content -->
    <section class="content">
            <div class="row">
        <div class="col-md-6">
          <!-- DONUT CHART -->
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Jumlah Pasien Laki-laki dan Perempuan</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <canvas id="pieChart" style="height:250px"></canvas>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col (LEFT) -->
        <div class="col-md-6">
          <!-- LINE CHART -->
          <!-- DONUT CHART -->
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Jumlah Pasien Dari Balita Sampai Dewasa</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <canvas id="pieChart1" style="height:250px"></canvas>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col (RIGHT) -->
      </div>
      <!-- /.row -->




      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Laporan Bulanan Klinik sehat teroos</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>NO</th>
                  <th>Tanggal</th>
                  <th>Balita</th>
                  <th>Anak-Anak</th>
                  <th>Remaja</th>
                  <th>Dewasa</th>
                  <th>Manula</th>
                  <th><center>Aksi</center></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                  <td>1</td>
                  <td>2019-02-20</td>
                  <td>10</td>
                  <td>15</td>
                  <td>5</td>
                  <td>7</td>
                  <td>13</td>
                  <td align="center"><button class="btn btn-primary"><i class="fa fa-edit"></i> Detail</button></td>
                </tr>
                <tr>
                  <td>1</td>
                  <td>2019-02-20</td>
                  <td>10</td>
                  <td>15</td>
                  <td>5</td>
                  <td>7</td>
                  <td>13</td>
                  <td align="center"><button class="btn btn-primary"><i class="fa fa-edit"></i> Detail</button></td>
                </tr>
                <tr>
                  <td>1</td>
                  <td>2019-02-20</td>
                  <td>10</td>
                  <td>15</td>
                  <td>5</td>
                  <td>7</td>
                  <td>13</td>
                  <td align="center"><button class="btn btn-primary"><i class="fa fa-edit"></i> Detail</button></td>
                </tr>
                <tr>
                  <td>1</td>
                  <td>2019-02-20</td>
                  <td>10</td>
                  <td>15</td>
                  <td>5</td>
                  <td>7</td>
                  <td>13</td>
                  <td align="center"><button class="btn btn-primary"><i class="fa fa-edit"></i> Detail</button></td>
                </tr>
                <tr>
                  <td>1</td>
                  <td>2019-02-20</td>
                  <td>10</td>
                  <td>15</td>
                  <td>5</td>
                  <td>7</td>
                  <td>13</td>
                  <td align="center"><button class="btn btn-primary"><i class="fa fa-edit"></i> Detail</button></td>
                </tr>
                <tr>
                  <td>1</td>
                  <td>2019-02-20</td>
                  <td>10</td>
                  <td>15</td>
                  <td>5</td>
                  <td>7</td>
                  <td>13</td>
                  <td align="center"><button class="btn btn-primary"><i class="fa fa-edit"></i> Detail</button></td>
                </tr>
                <tr>
                  <td>1</td>
                  <td>2019-02-20</td>
                  <td>10</td>
                  <td>15</td>
                  <td>5</td>
                  <td>7</td>
                  <td>13</td>
                  <td align="center"><button class="btn btn-primary"><i class="fa fa-edit"></i> Detail</button></td>
                </tr>
                <tr>
                  <td>1</td>
                  <td>2019-02-20</td>
                  <td>10</td>
                  <td>15</td>
                  <td>5</td>
                  <td>7</td>
                  <td>13</td>
                  <td align="center"><button class="btn btn-primary"><i class="fa fa-edit"></i> Detail</button></td>
                </tr>
                <tr>
                  <td>1</td>
                  <td>2019-02-20</td>
                  <td>10</td>
                  <td>15</td>
                  <td>5</td>
                  <td>7</td>
                  <td>13</td>
                  <td align="center"><button class="btn btn-primary"><i class="fa fa-edit"></i> Detail</button></td>
                </tr>
                <tr>
                  <td>1</td>
                  <td>2019-02-20</td>
                  <td>10</td>
                  <td>15</td>
                  <td>5</td>
                  <td>7</td>
                  <td>13</td>
                  <td align="center"><button class="btn btn-primary"><i class="fa fa-edit"></i> Detail</button></td>
                </tr>
                </tbody>
                <tfoot>
                <tr>
                  <th>NO</th>
                  <th>Tanggal</th>
                  <th>Balita</th>
                  <th>Anak-Anak</th>
                  <th>Remaja</th>
                  <th>Dewasa</th>
                  <th>Manula</th>
                  <th><center>Aksi</center></th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        
      </div>
      </section>
    <!-- /.content -->
  </div>

<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url('assetsLTE/bower_components/jquery/dist/jquery.min.js')?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('assetsLTE/bower_components/bootstrap/dist/js/bootstrap.min.js')?>"></script>
<!-- DataTables -->
<script src="<?php echo base_url('assetsLTE/bower_components/datatables.net/js/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assetsLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url('assetsLTE/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')?>"></script>
<!-- chartjs -->
<script src="<?php echo base_url('assetsLTE/bower_components/chart.js/Chart.js')?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('assetsLTE/bower_components/fastclick/lib/fastclick.js')?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assetsLTE/dist/js/adminlte.min.js')?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('assetsLTE/dist/js/demo.js')?>"></script>
<!-- page script -->
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
<script type="text/javascript">
   function tambahBuku() {
     var idfg = document.getElementById("idfg").value;
     var stre;
     var stre1;
     var asr=1;
     stre="<p id='srow" + idfg + "'><br><input type='text' class='form-control' name='keluhan[]'' placeholder='puyeng'>";
     $("#form").append(stre);

     stre1="<p id='srow1" + idfg + "'><br> <button class='btn btn-danger' onclick='hapusElemeng(\"#srow1" + idfg + "\");hapusElemeng(\"#srow" + idfg + "\"); return false;'><i class='fa fa-minus'></i></button>";
     $("#button").append(stre1);
     idfg = (idfg-1) + 2;
     document.getElementById("idfg").value = idfg;
   }
   function hapusElemeng(idfg) {
     $(idfg).remove();
   }
</script>

<script>
  $(function () {
    /* ChartJS
     * -------
     * Here we will create a few charts using ChartJS
     */

    //-------------
    //- PIE CHART1 -
    //--------------
    var pieChartCanvas = $('#pieChart1').get(0).getContext('2d')
    var pieChart       = new Chart(pieChartCanvas)
    var PieData        = [
      {
        value    : <?php echo $balita;?>,
        color    : '#d1d3cf',
        highlight: '#d1d3cf',
        label    : 'Balita'
      },
      {
        value    : <?php echo $anak;?>,
        color    : '#757081',
        highlight: '#757081',
        label    : 'Anak-Anak'
      },
      {
        value    : <?php echo $remaja;?>,
        color    : '#6c4f70',
        highlight: '#6c4f70',
        label    : 'Remaja'
      },
      {
        value    : <?php echo $dewasa;?>,
        color    : '#45415e',
        highlight: '#45415e',
        label    : 'Dewasa'
      },
      {
        value    : <?php echo $manula;?>,
        color    : '#39324b',
        highlight: '#39324b',
        label    : 'Manula'
      }
    ]
    var pieOptions     = {
      //Boolean - Whether we should show a stroke on each segment
      segmentShowStroke    : true,
      //String - The colour of each segment stroke
      segmentStrokeColor   : '#fff',
      //Number - The width of each segment stroke
      segmentStrokeWidth   : 2,
      //Number - The percentage of the chart that we cut out of the middle
      percentageInnerCutout: 50, // This is 0 for Pie charts
      //Number - Amount of animation steps
      animationSteps       : 100,
      //String - Animation easing effect
      animationEasing      : 'easeOutBounce',
      //Boolean - Whether we animate the rotation of the Doughnut
      animateRotate        : true,
      //Boolean - Whether we animate scaling the Doughnut from the centre
      animateScale         : false,
      //Boolean - whether to make the chart responsive to window resizing
      responsive           : true,
      // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio  : true,
      //String - A legend template
      legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'
    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    pieChart.Doughnut(PieData, pieOptions)
    //-------------
    //- PIE CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
    var pieChartCanvas = $('#pieChart').get(0).getContext('2d')
    var pieChart       = new Chart(pieChartCanvas)
    var PieData        = [
      {
        value    : <?php echo $perempuan;?>,
        color    : '#D81B60',
        highlight: '#D81B60',
        label    : 'Perempuan'
      },
      {
        value    : <?php echo $laki;?>,
        color    : 'red',
        highlight: 'red',
        label    : 'Laki-Laki'
      }
    ]
    var pieOptions     = {
      //Boolean - Whether we should show a stroke on each segment
      segmentShowStroke    : true,
      //String - The colour of each segment stroke
      segmentStrokeColor   : '#fff',
      //Number - The width of each segment stroke
      segmentStrokeWidth   : 2,
      //Number - The percentage of the chart that we cut out of the middle
      percentageInnerCutout: 50, // This is 0 for Pie charts
      //Number - Amount of animation steps
      animationSteps       : 100,
      //String - Animation easing effect
      animationEasing      : 'easeOutBounce',
      //Boolean - Whether we animate the rotation of the Doughnut
      animateRotate        : true,
      //Boolean - Whether we animate scaling the Doughnut from the centre
      animateScale         : false,
      //Boolean - whether to make the chart responsive to window resizing
      responsive           : true,
      // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio  : true,
      //String - A legend template
      legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'
    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    pieChart.Doughnut(PieData, pieOptions)
})
</script>

</body>
</html>