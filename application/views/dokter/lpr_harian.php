
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
   <section class="content-header">
        <h1><i class="fa fa-user-plus"></i> Laporan
        <i class="fa fa-angle-right"></i><small> Harian</small>
         </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Laporan Harian Klinik</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form method="post" action="<?php echo base_url(). 'laporan/harian'; ?>">
                    <label>Tanggal : </label>
                     <input type="date" name="tanggal" required="">
                    <input type="submit" value="OK" class="btn bg-purple  btn-xs">
              </form><br>
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>NO</th>
                  <th>Nama</th>
                  <th>Jenis Kelamin</th>
                  <th>Usia</th>
                  <th>Telp/HP</th>
                  <th><center>Aksi</center></th>
                </tr>
                </thead>
                <tbody>
                  <?php 
                  $n=1;
                  foreach ($periksa as $p) {
                   ?>
                <tr>
                  <td><?php echo $n++; ?></td>
                  <td><?php echo $p->nama; ?></td>
                  <td><?php if ( $p->jenis_kelamin=="L") {
                    echo "Laki-laki";
                  }elseif ( $p->jenis_kelamin=="P") {
                    echo "Peremppuan";
                  } ?></td>
                  <td><?php echo $p->usia; ?></td>
                  <td><?php echo $p->telp; ?></td>
                  <td align="center"><button class="btn btn-primary" data-toggle="modal" data-target="#modal-info" onclick="tampildata('<?php echo $p->ps_id ?>', '<?php echo $p->nama ?>', '<?php if ( $p->jenis_kelamin=="L") {
                    echo "Laki-laki";
                  }elseif ( $p->jenis_kelamin=="P") {
                    echo "Peremppuan";
                  } ?>', '<?php echo $p->usia ?>', '<?php echo $p->telp ?>', '<?php echo $p->alamat ?>', '<?php echo $p->gejala ?>')"><i class="fa fa-file-text"></i> Detail</button></td>
                </tr>
              <?php } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>NO</th>
                  <th>Nama</th>
                  <th>Jenis Kelamin</th>
                  <th>Usia</th>
                  <th>Telp/HP</th>
                  <th><center>Aksi</center></th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <div class="modal fade modal-info" id="modal-info" >
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                              <h4 class="modal-title">Detail buku</h4>
                            </div>
                            <div class="modal-body box-header">
                    <table>
                    <tr >
                    <td rowspan="7"><img src="<?php echo base_url('assetsLTE/dist/img/logo.png')?>" id="" width="100px" height="125px" ></td>
                    </tr>
                    <tr>
                    <td> <div class="col-sm-1"></div><b>ID PASIEN</b></td>
                    <td> : <span id="a"></span></td>
                    </tr>
                    <tr>
                    <td><div class="col-sm-1"></div><b>NAMA</b></td>
                    <td> : <span id="b"></span></td>
                    </tr>
                    <tr>
                    <td><div class="col-sm-1"></div><b>JENIS KELAMIN</b></td>
                    <td> : <span id="c"></span></td>
                    </tr>
                    <tr>
                    <td><div class="col-sm-1"></div><b>USIA </b></td>
                    <td> : <span id="d"></span></td>
                    </tr>
                    <tr>
                    <td><div class="col-sm-1"></div><b>NO.TELP </b></td>
                    <td> : <span id="e"></span></td>
                    </tr>
                    <tr>
                    <td><div class="col-sm-1"></div><b>ALAMAT </b></td>
                    <td> : <span id="f"></span></td>
                    </tr>
                    </table>
                    <br>
                    <table >
                    <tr>
                    <td><b>Gejala</b></td>
                    <td> : <span id="g"></span></td>
                    </tr>
                    </table>
                    
              </div>
              <div class="modal-footer" >
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
               </div>
            </div>
            <!-- /.modal-content -->
          </div>
      </div>
      </section>
    <!-- /.content -->
  </div>

<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url('assetsLTE/bower_components/jquery/dist/jquery.min.js')?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('assetsLTE/bower_components/bootstrap/dist/js/bootstrap.min.js')?>"></script>
<!-- DataTables -->
<script src="<?php echo base_url('assetsLTE/bower_components/datatables.net/js/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assetsLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url('assetsLTE/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('assetsLTE/bower_components/fastclick/lib/fastclick.js')?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assetsLTE/dist/js/adminlte.min.js')?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('assetsLTE/dist/js/demo.js')?>"></script>
<!-- page script -->

<script>
    function tampildata(ps_id, nama, jenis_kelamin, usia, telp, alamat, gejala){
       
      $('#a').html(ps_id);
      $('#b').html(nama);
      $('#c').html(jenis_kelamin);
      $('#d').html(usia);
      $('#e').html(telp);
      $('#f').html(alamat);
      $('#g').html(gejala);
      }
      
</script>
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
<script type="text/javascript">
   function tambahBuku() {
     var idfg = document.getElementById("idfg").value;
     var stre;
     var stre1;
     var asr=1;
     stre="<p id='srow" + idfg + "'><br><input type='text' class='form-control' name='keluhan[]'' placeholder='puyeng'>";
     $("#form").append(stre);

     stre1="<p id='srow1" + idfg + "'><br> <button class='btn btn-danger' onclick='hapusElemeng(\"#srow1" + idfg + "\");hapusElemeng(\"#srow" + idfg + "\"); return false;'><i class='fa fa-minus'></i></button>";
     $("#button").append(stre1);
     idfg = (idfg-1) + 2;
     document.getElementById("idfg").value = idfg;
   }
   function hapusElemeng(idfg) {
     $(idfg).remove();
   }
</script>
</body>
</html>