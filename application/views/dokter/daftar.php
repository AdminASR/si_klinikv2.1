  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1><i class="fa fa-user-plus"></i> Daftar
        <i class="fa fa-angle-right"></i><small> Data Pasien</small>
         </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
  <div class="col-md-9">
    <div class="box box-success">
            <div class="box-header with-border">
              <i class="fa fa-newspaper-o"></i>

              <h3 class="box-title">Form Pendaftaran Pasien|</h3> <a data-toggle="modal" data-target="#modal-daftar" href=""> Sudah Terdaftar?</a>
            </div>
            <form class="form-horizontal" method="post" action="<?php echo base_url('daftar/tambah_pasien') ?>">
              <div class="box-body">
                <div class="row">
                <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-4">Nama</label>

                  <div class="col-md-8">
                    <input type="text" class="form-control" name="nama" required="" placeholder="Nama Lengkap">
                    <input type="hidden" value="<?php echo date('Y-m-d') ?>" name="tgl_daftar">
                    <input type="hidden" value="<?php echo date('Y-m-d') ?>" name="tgl_periksa">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-4">Jenis Kelamin</label>

                  <div class="col-md-8">
                    <input type="radio"  name="jenis_kelamin"  value="L" required="" >Laki-laki
                    <input type="radio"  name="jenis_kelamin"  value="P" required="" >Perempuan
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-4">Usia(tahun)</label>

                  <div class="col-md-8">
                    <input type="number" class="form-control" name="usia" max="250" min="0" placeholder="123">
                  </div>
                </div>
              </div>
                <div class="col-md-6">
                  <div class="form-group">
                  <label class="col-md-3">Telp/HP</label>

                  <div class="col-md-8">
                    <input type="text" class="form-control" name="telp" placeholder="08XXXXXXXXX" >
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3">Alamat</label>

                  <div class="col-md-8">
                    <textarea class="form-control" rows="2" name="alamat" placeholder="Desa, Kecamatan, Kabupaten"></textarea>
                  </div>
                </div>
                </div>
              </div>
              <div class="form-group">
                  <label class="col-md-2">Keluhan</label>
                   <input id="idp" name='idp' value="1" type="hidden" >

                  <div class="col-md-4">
                    <input type="text" class="form-control" name="keluhan[]" placeholder="keluhan yg dialami pasien" >
                   <div id="form"></div>
                  </div>
                    <button class="btn btn-success " type="button" onclick="tambahBuku(); return false;"><i class="fa fa-plus"></i></button>
                    <div id="button"></div>
                </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-success pull-right">Simpan</button>
              </div>
              <!-- /.box-footer -->
            </form>
            <!-- /.box-body-->
          </div>
          <!-- /.box -->
  </div>
</div>
  <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php echo "$tertangani"; ?></h3>

              <p>Tertangani</p>
            </div>
            <div class="icon">
              <i class="fa fa-heartbeat"></i>
            </div>
          </div>
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?php echo "$antrian"; ?></h3>

              <p>Jumlah Antrian</p>
            </div>
            <div class="icon">
              <i class="fa fa-stethoscope"></i>
            </div>
          </div>
          <!-- small box -->
          <div class="small-box bg-purple">
            <div class="inner">
              <h3><?php echo "$laki"; ?></h3>

              <p>Pasien Laki-Laki</p>
            </div>
            <div class="icon">
              <i class="fa fa-male"></i>
            </div>
          </div>
          <div class="small-box bg-purple">
            <div class="inner">
              <h3><?php echo "$perempuan"; ?></h3>

              <p>Pasien Perempuan</p>
            </div>
            <div class="icon">
              <i class="fa fa-female"></i>
            </div>
          </div>
        </div>
  </div>
        <div class="modal modal-default fade" id="modal-daftar" >
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Masukkan Id Pasien</h4>
              </div>
              <div class="modal-body box-header">
              
            <!-- form start -->
            <form class="form-horizontal" action="#" method="post">
              <div class="box-body">
                <div class="form-group">
                  <label class="col-md-2">Id Pasien</label>

                  <div class="col-md-6">
                    <input type="text" class="form-control" name="id_pasien">
                  </div>
                  <div class="col-md-2">
                <button type="submit" class="btn btn-success pull-right">Kirim</button>
                </div>
                </div>
              </div>
            </form>
              </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
      </div>
        <!-- /.modal -->
      </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url('assetsLTE/bower_components/jquery/dist/jquery.min.js')?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('assetsLTE/bower_components/bootstrap/dist/js/bootstrap.min.js')?>"></script>
<!-- DataTables -->
<script src="<?php echo base_url('assetsLTE/bower_components/datatables.net/js/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assetsLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url('assetsLTE/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('assetsLTE/bower_components/fastclick/lib/fastclick.js')?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assetsLTE/dist/js/adminlte.min.js')?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('assetsLTE/dist/js/demo.js')?>"></script>
<!-- page script -->
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
<script type="text/javascript">
   function tambahBuku() {
     var idp = document.getElementById("idp").value;
     var stre;
     var stre1;
     var asr=1;
     stre="<p id='srow" + idp + "'><br><input type='text' class='form-control' name='keluhan[]'' placeholder='puyeng'>";
     $("#form").append(stre);

     stre1="<p id='srow1" + idp + "'><br> <button class='btn btn-danger' onclick='hapusElemeng(\"#srow1" + idp + "\");hapusElemeng(\"#srow" + idp + "\"); return false;'><i class='fa fa-minus'></i></button>";
     $("#button").append(stre1);
     idp = (idp-1) + 2;
     document.getElementById("idp").value = idp;
   }
   function hapusElemeng(idp) {
     $(idp).remove();
   }
</script>
</body>
</html>